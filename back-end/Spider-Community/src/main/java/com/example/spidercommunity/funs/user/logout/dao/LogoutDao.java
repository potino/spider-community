package com.example.spidercommunity.funs.user.logout.dao;

import com.example.spidercommunity.funs.user.logout.LogoutDto;
import org.apache.ibatis.annotations.Delete;

public interface LogoutDao {

    @Delete("delete from action_record where user_id = #{u_id} and action_id = 0")
    void deleteViewRecord(LogoutDto dto);
}
