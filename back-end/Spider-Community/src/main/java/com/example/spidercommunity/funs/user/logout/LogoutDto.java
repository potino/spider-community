package com.example.spidercommunity.funs.user.logout;

public class LogoutDto {
    private String u_id;

    public String getU_id() {
        return u_id;
    }

    public void setU_id(String u_id) {
        this.u_id = u_id;
    }
}
