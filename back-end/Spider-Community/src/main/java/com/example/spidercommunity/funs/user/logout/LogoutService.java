package com.example.spidercommunity.funs.user.logout;

public interface LogoutService {
    void deleteViewRecord(LogoutDto dto);
}
