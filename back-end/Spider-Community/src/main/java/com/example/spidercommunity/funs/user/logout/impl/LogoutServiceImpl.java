package com.example.spidercommunity.funs.user.logout.impl;

import com.example.spidercommunity.funs.user.logout.LogoutDto;
import com.example.spidercommunity.funs.user.logout.LogoutService;
import com.example.spidercommunity.funs.user.logout.dao.LogoutDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service // 说明这是一个受 spring 管理的业务类组件(Bean)
@Transactional // 说明本类所有方法都是事务性的
public class LogoutServiceImpl implements LogoutService {

    @Resource
    private LogoutDao logoutDao;

    @Override
    public void deleteViewRecord(LogoutDto dto) {
        logoutDao.deleteViewRecord(dto);
    }
}
