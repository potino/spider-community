package com.example.spidercommunity.funs.user.logout;

import com.example.spidercommunity.common.Result;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController // 说明这是一个受 spring 管理的对象，即 Bean 组件
@RequestMapping("/user/logout")
public class LogoutAPI {

    @Resource
    private LogoutService logoutService;

    @RequestMapping("")
    public Result checkPhone(@RequestBody LogoutDto dto){
        logoutService.deleteViewRecord(dto);
        return Result.success("成功退出！view record 删除！");
    }


}
