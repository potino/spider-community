package com.example.spidercommunity.funs.user.action.dao;

import com.example.spidercommunity.funs.user.action.ActionDto;
import com.example.spidercommunity.funs.user.action.Label;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface ActionDao {

    @Insert("insert into action_record values (#{post_id}, #{user_id}, 0, current_timestamp, 1)")
    void addViewRecord(ActionDto dto);

    @Select("select count(*) from action_record where user_id = #{user_id} and post_id = #{post_id} and action_id = 0")
    int checkViewRecord(ActionDto dto);

    @Update("update action_record set count = count+1, time = current_timestamp where user_id = #{user_id} and post_id = #{post_id} and action_id = 0")
    void refreshViewRecord(ActionDto dto);

    @Select("select label_id from post_label where post_id = #{post_id}")
    List<Label> getPostLabels(ActionDto dto);

    @Select("select count(user_id) from user_label where user_id = #{user_id} and label_id = #{label_id}")
    int checkUserHaveLabel(@Param("user_id") String user_id, @Param("label_id") int label_id);

    @Insert("insert into user_label values (#{user_id}, #{label_id}, 0, current_timestamp)")
    void createUserLabel(@Param("user_id") String user_id, @Param("label_id") int label_id);

    @Update("update user_label set score = score + 0.1 where user_id = #{user_id} and label_id = #{label_id}")
    void addClickScore(@Param("user_id") String user_id, @Param("label_id") int label_id);

    @Update("update user_label set score = score + 0.2 where user_id = #{user_id} and label_id = #{label_id}")
    void addLikeScore(@Param("user_id") String user_id, @Param("label_id") int label_id);

    @Update("update user_label set score = score + 0.3 where user_id = #{user_id} and label_id = #{label_id}")
    void addCommentScore(@Param("user_id") String user_id, @Param("label_id") int label_id);

    @Update("update user_label set score = score + 0.4 where user_id = #{user_id} and label_id = #{label_id}")
    void addCollectScore(@Param("user_id") String user_id, @Param("label_id") int label_id);
}
