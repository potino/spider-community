package com.example.spidercommunity.funs.user.action.impl;

import com.example.spidercommunity.funs.user.action.ActionDto;
import com.example.spidercommunity.funs.user.action.ActionService;
import com.example.spidercommunity.funs.user.action.Label;
import com.example.spidercommunity.funs.user.action.dao.ActionDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class ActionServiceImpl implements ActionService {
    @Resource
    private ActionDao actionDao;

    @Override
    public void addViewRecord(ActionDto dto) {
        if (actionDao.checkViewRecord(dto) >= 1)
            actionDao.refreshViewRecord(dto);
        else actionDao.addViewRecord(dto);
    }

    @Override
    public void addClickRecord(ActionDto dto) {
        List<Label> labelList = new ArrayList<>();
        labelList.addAll(actionDao.getPostLabels(dto));
        for (int i = 0; i < labelList.size(); i++){
            String user_id = dto.getUser_id();
            int label_id = labelList.get(i).getLabel_id();
            int count = actionDao.checkUserHaveLabel(user_id, label_id);
            if (count == 0)
                actionDao.createUserLabel(user_id,label_id);
            actionDao.addClickScore(user_id, label_id);
        }


    }

    @Override
    public void addLikeRecord(ActionDto dto) {
        List<Label> labelList = new ArrayList<>();
        labelList.addAll(actionDao.getPostLabels(dto));
        for (int i = 0; i < labelList.size(); i++){
            String user_id = dto.getUser_id();
            int label_id = labelList.get(i).getLabel_id();
            int count = actionDao.checkUserHaveLabel(user_id, label_id);
            if (count == 0)
                actionDao.createUserLabel(user_id,label_id);
            actionDao.addLikeScore(user_id, label_id);
        }

    }

    @Override
    public void addCommentRecord(ActionDto dto) {
        List<Label> labelList = new ArrayList<>();
        labelList.addAll(actionDao.getPostLabels(dto));
        for (int i = 0; i < labelList.size(); i++){
            String user_id = dto.getUser_id();
            int label_id = labelList.get(i).getLabel_id();
            int count = actionDao.checkUserHaveLabel(user_id, label_id);
            if (count == 0)
                actionDao.createUserLabel(user_id,label_id);
            actionDao.addCommentScore(user_id, label_id);
        }
    }

    @Override
    public void addCollectRecord(ActionDto dto) {
        List<Label> labelList = new ArrayList<>();
        labelList.addAll(actionDao.getPostLabels(dto));
        for (int i = 0; i < labelList.size(); i++){
            String user_id = dto.getUser_id();
            int label_id = labelList.get(i).getLabel_id();
            int count = actionDao.checkUserHaveLabel(user_id, label_id);
            if (count == 0)
                actionDao.createUserLabel(user_id,label_id);
            actionDao.addCollectScore(user_id, label_id);
        }
    }
}
