package com.example.spidercommunity.funs.admin.users.dao;
import com.example.spidercommunity.funs.admin.users.AddLabelDto;
import com.example.spidercommunity.funs.admin.users.UidAndLabelId;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.web.bind.annotation.RequestBody;

public interface AddDao {
    @Select("select count(*) from label_ where label_name=#{name}" )
    int check(@RequestBody AddLabelDto dto);

    @Select("select count(*) from user_label where label_id=#{label_id} and user_id=#{user_id}" )
    int checkTwo(@RequestBody UidAndLabelId dto);

    @Insert("INSERT INTO user_label VALUES ( #{user_id} , #{label_id} , #{score} , #{time} )")
    int add(@RequestBody UidAndLabelId dto);

    @Select("select label_id from label_ where label_name=#{name}")
    int getId(AddLabelDto dto);
}
