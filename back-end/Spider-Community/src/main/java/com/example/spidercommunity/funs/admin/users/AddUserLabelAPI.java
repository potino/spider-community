package com.example.spidercommunity.funs.admin.users;


import com.example.spidercommunity.common.Result;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/admin/users/addLabel")
public class AddUserLabelAPI {
    @Resource
    private AddService addService;

    @RequestMapping("")
    public Result addLabel(@RequestBody AddLabelDto dto){
       String result= addService.add(dto);
    return Result.success(result);
    }
}
