package com.example.spidercommunity.funs.admin.users.impl;

import com.example.spidercommunity.funs.admin.users.AddLabelDto;
import com.example.spidercommunity.funs.admin.users.AddService;
import com.example.spidercommunity.funs.admin.users.UidAndLabelId;
import com.example.spidercommunity.funs.admin.users.dao.AddDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
@Transactional
public class AddServiceImpl implements AddService {
    @Resource
    private AddDao addDao;


    @Override
    public String add(AddLabelDto dto) {
        int cnt=addDao.check(dto);
        System.out.println(dto.getUid());
        System.out.println(dto.getName());
        if (cnt<1)return "无该标签";
        else {


            int label=addDao.getId(dto);
            UidAndLabelId a=new UidAndLabelId(dto.getUid(),label);
            if (addDao.checkTwo(a)>0)return "该用户已有该标签";
            addDao.add(a);
            return "添加成功";
        }
    }
}
