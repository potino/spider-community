package com.example.spidercommunity.funs.admin.users;


import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;


public class UidAndLabelId {
    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public UidAndLabelId(int id, int label){
        user_id=id;
        label_id=label;
        score=0;
        time= new java.sql.Date(new java.util.Date().getTime());
    }


    private  int user_id;
    private  int label_id;
    private double score ;

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getLabel_id() {
        return label_id;
    }

    public void setLabel_id(int label_id) {
        this.label_id = label_id;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }


    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date time;


}
