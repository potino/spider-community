package com.example.spidercommunity.funs.user.action;

public interface ActionService {
    void addViewRecord(ActionDto dto);

    void addClickRecord(ActionDto dto);

    void addLikeRecord(ActionDto dto);

    void addCommentRecord(ActionDto dto);

    void addCollectRecord(ActionDto dto);
}
