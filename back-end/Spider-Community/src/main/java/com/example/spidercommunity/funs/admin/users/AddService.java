package com.example.spidercommunity.funs.admin.users;

public interface AddService {

    String add(AddLabelDto dto);
}
